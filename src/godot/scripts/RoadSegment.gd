extends Spatial

export var length = 1

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$RoadMeshInstance.mesh.size = Vector2(length, 2)
	$RoadMeshInstance.mesh.material.set("shader_param/Length", length)

	$SidewalkMeshInstance.mesh.size.z = length
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
