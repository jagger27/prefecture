extends CSGBox


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var max_height: float
export var max_width: float
export var max_depth: float

export var min_height: float
export var min_width: float
export var min_depth: float

# Called when the node enters the scene tree for the first time.
func _ready():
	max_height = height
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
