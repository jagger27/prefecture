# Procedural Generation

## Configurable structures
What if instead of designing a static mesh for every single type of building you could define a set of parameters that control the size and shape of a building?

Most textures are designed to tile seamlessly, so an apartment building should have no problem growing an extra floor or two taller or a few windows wider. A designer can set a range of valid sizes for each design and how they are allowed to interact with their surroundings.

Simpler operations like mirroring and texture recolouring can add some variety too.

Example levers for building designers:
- structure footprint
- structure height
- lot line setbacks (how close a building wants to be to its property line)
- allowable props (air conditioners, antennas, lamps, etc.)
- prop attachment points
- repeatable meshes (puzzle pieces)
- stretchable meshes
- texture/shader parameters 
- complex enclaves (lots inside of lots, like a shopping mall or university campus)

Example gameplay levers:
- mixed use buildings (street level commercial below residential apartments, for example)
- housing capacity
- wealth class
- resource usage